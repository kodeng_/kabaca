import React, { useState } from "react";
import { StyleSheet, View, Modal, TouchableHighlight } from "react-native";
import { Text, Button, Overlay } from "react-native-elements";

const AboutMe = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={{ textAlign: "center" }} h4>
        About Me
      </Text>
      <View style={{ margin: 10 }}>
        <Button title="Apaya" onPress={() => navigation.navigate("Login")} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
});

export default AboutMe;
