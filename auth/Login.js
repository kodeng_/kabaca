import React, { Component, useEffect, useState } from "react";
import { StyleSheet, View, Alert, TouchableHighlight } from "react-native";
import { Text, Button, Input } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/FontAwesome5";
import axios from "axios";

// const Login = ({ props, navigation }) => {
//   const [username, setUsername] = useState("");
//   const [password, setPassword] = useState("");
//   const [token, setToken] = useState();

//   loginValidation = () => {
//     if (username == "") {
//       Alert.alert("Error!", "Please fill username");
//       return false;
//     } else if (password == "") {
//       Alert.alert("Error!", "Please fill password");
//       return false;
//     }
//     return true;
//   };

//   const storeToken = async (data) => {
//     try {
//       await AsyncStorage.setItem("userData", data);
//       // console.log();
//     } catch (error) {
//       console.log("Something went wrong", error);
//     }
//   };

//   successLogin = async () => {
//     axios
//       .post("https://user-article.herokuapp.com/api/auth/signin", {
//         username: username,
//         password: password,
//       })
//       .then((res) => {
//         // console.log(res);
//         console.log(res.data);
//         setToken({
//           token: res.data.accessToken,
//         });
//         storeToken(res.data.accessToken);
//         console.log("udah disave", token);
//         navigation.navigate("Dashboard", {
//           usernameParams: username,
//           passwordParams: password,
//         });
//       })
//       .catch(function (error) {
//         console.log(error);
//         Alert.alert(error + "", "Your username and password does't match");
//       });
//   };

//   useEffect(() => {
//     const perfectLogin = async () => {
//       console.log("tes", token);
//       const perfect = await AsyncStorage.getItem("userData");
//       console.log("ini perfect : ", perfect);
//       if (perfect !== null) {
//         navigation.navigate("Dashboard");
//       }
//     };
//     perfectLogin;
//   }, []);

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }

  loginValidation = () => {
    const { username, password } = this.state;
    if (username == "") {
      Alert.alert("Error!", "Please fill username");
      return false;
    } else if (password == "") {
      Alert.alert("Error!", "Please fill password");
      return false;
    }
    return true;
  };

  async storeToken(data) {
    try {
      await AsyncStorage.setItem("userData", data);
      console.log();
    } catch (error) {
      console.log("Something went wrong", error);
    }
  }

  successLogin = async () => {
    if (this.loginValidation()) {
      axios
        .post("https://user-article.herokuapp.com/api/auth/signin", {
          username: this.state.username,
          password: this.state.password,
        })
        .then((res) => {
          // console.log(res);
          console.log(res.data);
          this.setState({
            token: res.data.accessToken,
          });
          this.storeToken(res.data.accessToken);
          console.log("ini token", res.data.accessToken);
          this.props.navigation.navigate("Dashboard", {
            screen: "Dashboard",
            params: {
              screen: "Dashboard",
              params: {
                usernameParams: this.state.username,
                passwordParams: this.state.password,
              },
            },
          });
        })
        .catch(function (error) {
          console.log(error);
          Alert.alert(error + "", "Your username and password does't match");
        });
    }
  };

  componentDidMount() {
    const _bootstrapAsync = async () => {
      const masuk = await AsyncStorage.getItem("userData");
      console.log("ini token", masuk);
      if (masuk !== null) {
        this.props.navigation.navigate("Dashboard", {
          screen: "Dashboard",
          params: {
            screen: "Dashboard",
          },
        });
      }
    };
    _bootstrapAsync();
  }

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView>
          <View>
            <Text style={{ textAlign: "center" }} h4>
              Login
            </Text>
            <Input
              placeholder="Username"
              autoCapitalize="none"
              onChangeText={(value) => this.setState({ username: value })}
              leftIcon={{ type: "font-awesome5", name: "email" }}
            />
            <Input
              placeholder="Password"
              onChangeText={(value) => this.setState({ password: value })}
              leftIcon={{ type: "font-awesome5", name: "lock" }}
              secureTextEntry={true}
            />
          </View>
          <View style={{ marginHorizontal: 10 }}>
            <Button title="Login" onPress={() => this.successLogin()} />

            <Text
              style={{ color: "blue", textAlign: "center", paddingTop: 15 }}
              onPress={() => this.props.navigation.navigate("Register")}
            >
              Create a new Account
            </Text>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
});

export default Login;
