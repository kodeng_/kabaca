import React, { useState } from "react";
import { StyleSheet, View, TouchableHighlight, Alert } from "react-native";
import { Text, Button, Input } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import Icon from "react-native-vector-icons/FontAwesome5";
import axios from "axios";

const Register = ({ navigation }) => {
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setConfirmation] = useState("");

  const validationRegister = () => {
    const re = /\S+@\S+\.\S+/;
    if (name == "") {
      Alert.alert("Error!", "please fill your name");
    } else if (username == "") {
      Alert.alert("Error!", "Please fill username");
      return false;
    } else if (email == "") {
      Alert.alert("Error!", "Please fill email");
      return false;
    } else if (!re.test(email)) {
      Alert.alert("Error!", "Your email does't email format");
      return false;
    } else if (password == "") {
      Alert.alert("Error!", "Please fill password");
      return false;
    } else if (passwordConfirmation == "") {
      Alert.alert("Error!", "Please fill confirm password");
      return false;
    } else if (password != passwordConfirmation) {
      Alert.alert("Error!", "Your password and confirm password is not match");
      return false;
    }
    return true;
  };

  const successRegister = async () => {
    if (validationRegister()) {
      axios
        .post("https://user-article.herokuapp.com/api/auth/signup", {
          name: name,
          username: username,
          email: email,
          password: password,
          passwordConfirmation: passwordConfirmation,
        })
        .then((res) => {
          console.log("ini apaya : ", res);

          console.log("ini upuyu : ", res.data);
          Alert.alert("Holla Kabaca Friends!", "Succesfully Register");
          navigation.navigate("Login");
        });
    }
  };

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View>
          <Text style={{ textAlign: "center" }} h4>
            Register
          </Text>
          <Input
            placeholder="Nama"
            onChangeText={(name) => setName(name)}
            leftIcon={<Icon name="address-card" size={20} />}
          />
          <Input
            placeholder="Username"
            autoCapitalize="none"
            onChangeText={(username) => setUsername(username)}
            leftIcon={<Icon name="user-circle" size={20} />}
          />
          <Input
            placeholder="Email"
            autoCapitalize="none"
            onChangeText={(email) => setEmail(email)}
            leftIcon={{ type: "font-awesome5", name: "email" }}
          />
          <Input
            placeholder="Password"
            leftIcon={<Icon name="lock" size={20} />}
            onChangeText={(password) => setPassword(password)}
            secureTextEntry={true}
          />
          <Input
            placeholder="Confirm Password"
            leftIcon={<Icon name="unlock" size={20} />}
            onChangeText={(passwordConfirmation) =>
              setConfirmation(passwordConfirmation)
            }
            secureTextEntry={true}
          />
        </View>
        <View style={{ marginHorizontal: 10 }}>
          <Button title="Register" onPress={() => successRegister()} />

          <Text
            onPress={() => navigation.navigate("Login")}
            style={{ color: "blue", textAlign: "center", paddingTop: 15 }}
          >
            I have an account
          </Text>
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
});

export default Register;
