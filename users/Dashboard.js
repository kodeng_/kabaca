import React from "react";
import { View, Alert, StyleSheet } from "react-native";
import { Text, Button, Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";

const Dashboard = ({ navigation }) => {
  const successLogout = () => {
    AsyncStorage.removeItem("userData");
    navigation.navigate("Home");
    Alert.alert("Nice to meet you!", "Successfully Logout.");
  };

  return (
    <View style={styles.container}>
      <Header
        leftComponent={{ icon: "menu", color: "#fff" }}
        centerComponent={{ text: "Dashboard", style: { color: "#fff" } }}
        rightComponent={{
          icon: "add",
          color: "#fff",
          onPress: () => navigation.navigate("Writer"),
        }}
      />
      <Text style={{ textAlign: "center" }} h5>
        Hallo Writer
      </Text>
      <View style={styles.header}>
        <View style={styles.buton}>
          <Button
            title="Articles"
            onPress={() => navigation.navigate("Information")}
          />
        </View>
        <View style={styles.buton}>
          <Button
            title="Logout"
            onPress={() => successLogout()}
            type="outline"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    flex: 1,

    justifyContent: "center",
  },
  buton: {
    marginHorizontal: 20,
    marginVertical: 5,
    alignContent: "center",
    justifyContent: "center",
  },
});

export default Dashboard;
