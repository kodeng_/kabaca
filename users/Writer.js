import React, { useEffect, useState } from "react";
import { StyleSheet, View, Alert } from "react-native";
import { Text, Button, Header, Card, Input } from "react-native-elements";
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

const Writer = ({ navigation }) => {
  const [token, setToken] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const getToken = async (data) => {
    try {
      setToken(await AsyncStorage.getItem("userData", data));
      console.log("write token : ", token);
    } catch (error) {
      console.log("write error : ", error);
    }
  };

  useEffect(() => {
    console.log("munculkah token apakah? ", token);
    getToken();
  }, [token]);

  const successArtikel = async () => {
    const header = {
      authorization: "Bearer " + token,
    };
    const body = {
      title: title,
      content: content,
    };
    axios
      .post("https://user-article.herokuapp.com/api/articles", body, {
        headers: header,
      })
      .then((res) => {
        console.log("ini upuyu : ", res.data);
        Alert.alert("Holla Kabaca Friends!", "Succesfully Register");
        navigation.navigate("Login");
      });
  };

  return (
    <View style={styles.container}>
      <Header
        leftComponent={{ icon: "menu", color: "#fff" }}
        centerComponent={{ text: "MY TITLE", style: { color: "#fff" } }}
        rightComponent={{
          icon: "home",
          color: "#fff",
          onPress: () => navigation.navigate("Dashboard"),
        }}
      />
      <Card>
        <Card.Title>Add Article</Card.Title>
        <Card.Divider />
        <Input
          placeholder="Title"
          multiline={true}
          numberOfLines={2}
          onChangeText={(title) => setTitle(title)}
        />
        <Input
          placeholder="Content"
          multiline={true}
          numberOfLines={10}
          onChangeText={(title) => setTitle(title)}
        />
      </Card>
      <View style={styles.button}>
        <Button title="Publish Article" onPress={() => successArtikel()} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  button: {
    margin: 15,
  },
});

export default Writer;
