import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import { Text, Card, Header } from "react-native-elements";
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from "react-native-gesture-handler";

const Movie = () => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetch(
      "https://api.themoviedb.org/3/movie/popular?api_key=ecb397c7dcb6907a1424ccd887cd8277&language=en-US&page=1"
    )
      .then((respone) => respone.json())
      .then((json) => setData(json.results))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        keyExtractor={({ id }) => id.toString()}
        renderItem={({ item }) => (
          <ScrollView>
            <TouchableOpacity>
              <Card>
                <Card.Title>{item.original_title}</Card.Title>
                <Card.Divider>
                  <Text>{item.overview}</Text>
                </Card.Divider>
              </Card>
            </TouchableOpacity>
          </ScrollView>
        )}
      />
      <StatusBar style="auto" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  tema: {
    flex: 1,
    alignItems: "center",
    paddingTop: 10,
  },
});

export default Movie;
