import React from "react";
import { StyleSheet, View } from "react-native";
import { Text, Button, Header, Card } from "react-native-elements";

const Information = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Header
        leftComponent={{ icon: "menu", color: "#fff" }}
        centerComponent={{ text: "Information", style: { color: "#fff" } }}
        rightComponent={{
          icon: "home",
          color: "#fff",
          onPress: () => navigation.navigate("Dashboard"),
        }}
      />
      <View style={styles.content}>
        <Text>Informasi</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  content: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default Information;
