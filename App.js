import React from "react";
import Navigation from "./components/Navigation";
import { ThemeProvider } from "react-native-elements";

export default function App() {
  return (
    <ThemeProvider>
      <Navigation />
    </ThemeProvider>
  );
}
