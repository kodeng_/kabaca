import React, { useState, useEffect } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import { Text, Input } from "react-native-elements";
import axios from "axios";
import { useIsFocused } from "@react-navigation/native";

const Artikel = ({ route }) => {
  const { idwoy } = route.params;
  const isFocused = useIsFocused();
  const [data, setData] = useState({
    user: { id: 0, name: "" },
    title: "",
    content: "",
    createdAt: "",
  });
  console.log("id : " + idwoy);

  const postDetail = async (e) => {
    // console.log("alahhh");
    axios
      .get(`https://user-article.herokuapp.com/api/articles/${idwoy}`)
      .then(function (res) {
        setData(res.data.article);
        console.log(res.data.article);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    postDetail();
  }, []);

  return (
    <>
      <View>
        <ScrollView>
          <Text style={{ textAlign: "center", marginTop: 10 }} key={data.id} h3>
            {data.title}
          </Text>

          <Text style={styles.content}>{data.content}</Text>
          <Text style={styles.title}>Writer : {data.user.name}</Text>
          <Text style={styles.title}>
            Created at : {data.createdAt.substring(0, 10)}
          </Text>
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
  },
  content: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
});
export default Artikel;
