import React, { useEffect, useState } from "react";
import {
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  View,
  Modal,
  FlatList,
  Button,
} from "react-native";
import { Text, Card, Header } from "react-native-elements";
import axios from "axios";
import Navigation from "./Navigation";

const Home = ({ navigation }) => {
  const [data, setData] = useState([{ id: 0 }]);
  const [modalVisible, setModalVisible] = useState(false);

  const postArtikel = async () => {
    await axios
      .get("https://user-article.herokuapp.com/api/articles")
      .then((res) => {
        // console.log(res);
        setData(res.data.article);
      })
      .catch((error) => {
        console.log(error.res.data, null, 2);
      });
  };

  useEffect(() => {
    postArtikel();
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        {data.length === 0 ? (
          <Text> NO DATA </Text>
        ) : (
          data.map((artikel) => {
            return (
              <>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate("Artikel", {
                      idwoy: artikel.id,
                    })
                  }
                >
                  <Card style={{ paddingBottom: 30 }}>
                    <Card.Title h4>{artikel.title}</Card.Title>
                    <Card.Divider style={{ marginBottom: 50 }}>
                      <Text style={styles.cardtext} numberOfLines={3}>
                        {artikel.content}
                      </Text>
                    </Card.Divider>
                  </Card>
                </TouchableOpacity>
              </>
            );
          })
        )}
      </ScrollView>
      {/* <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.modal}>
          <View style={styles.modalView}>
            <Card style={{ paddingBottom: 30 }}>
              <Card.Title h4>{title}</Card.Title>
              <Card.Divider style={{ marginBottom: 50 }}>
                <Text style={styles.cardtext} numberOfLines={3}>
                  {content}
                </Text>
              </Card.Divider>
            </Card>
            <Button
              title="Hide Modal"
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            />
          </View>
        </View>
      </Modal> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  tema: {
    flex: 1,
    alignItems: "center",
    paddingTop: 10,
  },
  cardtext: {
    paddingTop: 5,
    marginBottom: 10,
  },
  modal: {
    justifyContent: "center",
  },
  modalView: {
    backgroundColor: "skyblue",
    alignItems: "center",
  },
});

export default Home;
