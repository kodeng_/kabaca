import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/FontAwesome5";
import Home from "./Home";
import Auth from "../auth/Auth";
import Information from "../users/Information";
import Movie from "../users/Movie";
import Login from "../auth/Login";
import Register from "../auth/Register";
import Artikel from "./Artikel";
import Dashboard from "../users/Dashboard";
import Writer from "../users/Writer";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const TabUser = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={TabScreen}
          options={{ title: "Kabaca" }}
        />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Artikel" component={Artikel} />
        <Stack.Screen
          name="Dashboard"
          component={DrawerScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const TabScreen = () => {
  return (
    <Tab.Navigator tabBarOptions={{ style: { paddingBottom: 10 } }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{ tabBarIcon: () => <Icon name="home" /> }}
      />
      <Tab.Screen
        name="I am"
        component={Auth}
        options={{ tabBarIcon: () => <Icon name="smile" /> }}
      />
    </Tab.Navigator>
  );
};

const DrawerScreen = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen name="Writer" component={Writer} />
      <Drawer.Screen name="Information" component={Information} />
    </Drawer.Navigator>
  );
};

// const TabUserScreen = () => {
//   <TabUser.Navigator>
//     <TabUser.Screen name="Dashboard" component={DrawerScreen} />
//     <TabUser.Screen name="Movie" component={Movie} />
//   </TabUser.Navigator>;
// };

export default Navigation;
